//  ASJToggleButton.m
//
// Copyright (c) 2015 Sudeep Jaiswal
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#import "ASJToggleButton.h"

@interface ASJToggleButton ()

@property (nonatomic) ASJButtonState asj_buttonState;

- (void)setUp;
- (void)toggle;
- (void)fireToggleBlock;

@end

@implementation ASJToggleButton

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
      [ASJToggleButton buttonWithType:UIButtonTypeCustom];
    }
    return self;
}

+ (instancetype)buttonWithType:(UIButtonType)buttonType {
	ASJToggleButton *button = [super buttonWithType:UIButtonTypeCustom];
	[button setUp];
	return button;
}

- (void)setUp {
	self.selected = NO;
	self.asj_buttonState = ASJButtonStateOff;
}

- (void)toggle {
  self.selected = !self.selected;
  if (self.selected) {
    self.asj_buttonState = ASJButtonStateOn;
    [self fireToggleBlock];
    return;
  }
  self.asj_buttonState = ASJButtonStateOff;
  [self fireToggleBlock];
}

- (void)fireToggleBlock {
  if (_toggleBlock) {
    _toggleBlock(self, _asj_buttonState);
  }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
  [self toggle];
  [super touchesEnded:touches withEvent:event];
}


#pragma mark - Property overrides

- (void)setAsj_offImage:(UIImage *)asj_offImage {
	[self setImage:asj_offImage forState:UIControlStateNormal];
}

- (void)setAsj_onImage:(UIImage *)asj_onImage {
	[self setImage:asj_onImage forState:UIControlStateSelected];
}

@end
