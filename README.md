# ASJToggleButton
This is a UIButton subclass that provides the functionality to switch between on and off states. The toggle function is available for UIButton by default but it's not immediately obvious how to use it. You can set the title(s) and or image(s) for both states and use the `selected` property to toggle.

![alt tag](Images/Screenshot.png)

This class makes it easier to do so. You have two properties to set the on and off images for the corresponding states:

```
@property (nonatomic) IBInspectable UIImage *asj_offImage;
```

```
@property (nonatomic) IBInspectable UIImage *asj_onImage;
```

Both properties are IBInspectable which means that they can be set using the interface builder of your choice; Xibs or Storyboards.

![alt tag](Images/IBInspectable.png)

###To-do
- Add titles for on and off states

# License

ASJToggleButton is available under the MIT license. See the LICENSE file for more info.
