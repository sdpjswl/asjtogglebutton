//
//  ViewController.m
//  ASJToggleButtonExample
//
//  Created by sudeep on 03/05/15.
//  Copyright (c) 2015 Sudeep Jaiswal. All rights reserved.
//

#import "ViewController.h"
#import "ASJToggleButton.h"

@interface ViewController ()

@property (nonatomic) IBOutlet UIView *buttonContainer;
@property (nonatomic) IBOutlet UILabel *buttonIsLabel;

- (void)setUpButton;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self setUpButton];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setUpButton {
    ASJToggleButton *button = [ASJToggleButton buttonWithType:UIButtonTypeCustom];
    button.frame = _buttonContainer.bounds;
    button.asj_onImage = [UIImage imageNamed:@"checked"];
    button.asj_offImage = [UIImage imageNamed:@"unchecked"];
    [button addTarget:self action:@selector(tapped:) forControlEvents:UIControlEventTouchUpInside];
    [_buttonContainer addSubview:button];
}

- (void)tapped:(ASJToggleButton *)sender {
    if (sender.asj_buttonState) {
        _buttonIsLabel.text = @"Button is... ON";
        return;
    }
    _buttonIsLabel.text = @"Button is... OFF";
}

@end
